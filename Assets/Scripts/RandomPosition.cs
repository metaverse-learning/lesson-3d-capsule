﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPosition : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        gameObject.transform.position = new Vector3(Random.Range(-5.0f, 5.0f), 7, Random.Range(-5.0f, 5.0f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
